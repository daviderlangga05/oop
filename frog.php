<?php
require('index.php');

class Frog extends Animal
{
  public $legs = 4;

  public function jump()
  {
    echo "hop hop";
  }
}

$kodok = new Frog("buduk");
$kodok->jump(); // "hop hop"
